# Changelog

<!--next-version-placeholder-->

## v0.6.1 (2022-03-01)
### Fix
* Config now generates a new API key ([`88a9c4f`](https://gitlab.com/loci-notes/loci-cli/-/commit/88a9c4faef337c31d0d0e1cd9be51ad6ad693c5b))

### Documentation
* Add link to README ([`0494a53`](https://gitlab.com/loci-notes/loci-cli/-/commit/0494a53bb57ba65a88fb689ac922d7a3e51dcb5f))

## v0.6.0 (2022-01-04)
### Feature
* Added support for note types in output ([`0199227`](https://gitlab.com/loci-notes/loci-cli/-/commit/0199227cfdfb4a550825fb171a8fdda7579b459b))

## v0.4.0 (2021-06-14)
### Feature
* Detailed user info and remove regex ([`087624d`](https://gitlab.com/loci-notes/loci-cli/-/commit/087624d28a08e454e47870453104cc1ac40345be))

## v0.3.0 (2021-05-25)
### Feature
* Add fatal param to error msg ([`381ce62`](https://gitlab.com/loci-notes/loci-cli/-/commit/381ce6202102723cf38a9d6b6ba5f1742775bb37))

## v0.2.3 (2021-05-25)
### Fix
* Status correctly reported ([`f52c7b7`](https://gitlab.com/loci-notes/loci-cli/-/commit/f52c7b7363f5a74ee8f747800d62b2ee0a065507))

## v0.2.2 (2021-05-25)
### Fix
* Correct short options for artifacts ([`a3b47bb`](https://gitlab.com/loci-notes/loci-cli/-/commit/a3b47bb1bd00137ec95d0a77ebcfc0d80ebf7750))

## v0.2.1 (2021-05-20)
### Fix
* Corrected request content type on submit ([`fe5a0fa`](https://gitlab.com/loci-notes/loci-cli/-/commit/fe5a0faf95aa802287791c0133f6cbb1d3554eb2))

### Documentation
* Readme update for install ([`8578360`](https://gitlab.com/loci-notes/loci-cli/-/commit/857836022bdd12770ba8c4ad96e5b00965b17d74))

## v0.2.0 (2021-05-19)
### Feature
* Added version cmd ([`dba5959`](https://gitlab.com/loci-notes/loci-cli/-/commit/dba5959899d5eced3c38b0e2e18d5adfb936dda9))

## v0.1.0 (2021-05-17)
### Feature
* Add artifact querying ([`bf4657d`](https://gitlab.com/loci-notes/loci-cli/-/commit/bf4657dbf7a517fdc8d27b98c3d1d7d3caaa7e25))
