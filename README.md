# Loci CLI
The official Loci CLI tool. Performs basic Loci Notes tasks from any command line.

## Docs
https://loci-notes.gitlab.io/clients/cli

## Installation
### Standard
`pip3 install loci-cli`

### Latest
`pip3 install git+https://gitlab.com/loci-notes/loci-cli`

### Development
* It is recommended that you use a virtualenv for development:
    * `virtualenv --python python3 venv`
    * `source ./venv/bin/activate`
* `python setup.py develop`
* Run with `loci`
