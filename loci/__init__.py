# Application global vars
__version__ = "0.6.1"
PROG_NAME = "loci"
PROG_DESC = "The official Loci CLI tool. Performs basic Loci Notes tasks from any command line."
PROG_EPILOG = "Written by TheTwitchy"
DEBUG = True
